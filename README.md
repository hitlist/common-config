## Allstar common config

This project contains configuration and setup file for setting up an enviornment.

### Compose

Contains a compose file for starting required services to enable testing and micro-service development

* 3 Node RethinkDB cluster and a single proxy node
* 3 Node NATS cluster
* 1 Jaeger tracing server
* 1 allstar-auth instance
* 1 allstar-activity instance

```bash
docker compose -f common-config/compose/backend.yml up -d
```
