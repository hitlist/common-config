\connect allstar;
-- CREATE TABLES TO ALLOW NAMED CRON JOBS
-- https://github.com/citusdata/pg_cron/issues/24#issuecomment-316636466

BEGIN;

CREATE TABLE cron.job_name (
  id serial PRIMARY KEY,
  jobid bigint REFERENCES cron.job (jobid),
  name text NOT NULL UNIQUE
);

GRANT SELECT ON cron.job_name TO allstar;

CREATE OR REPLACE FUNCTION cron.schedule_named(
  name text,
  schedule text,
  command text
) RETURNS bigint AS $$
#variable_conflict use_variable
DECLARE
  result bigint;
BEGIN
  SELECT cron.schedule(schedule, command) INTO result;
  INSERT INTO cron.job_name (jobid, name) VALUES (result, name);
  RETURN result;
END;
$$ LANGUAGE plpgsql STRICT SECURITY DEFINER;

CREATE OR REPLACE FUNCTION cron.unschedule_named(
  name text
) RETURNS boolean AS $$
#variable_conflict use_variable
DECLARE
  jobid bigint;
  result boolean;
BEGIN
  SELECT j.jobid INTO jobid
    FROM cron.job_name j
    WHERE j.name = name;

  IF jobid IS NULL THEN
    RAISE EXCEPTION 'Unknown job with name %', name;
  END IF;

  SELECT cron.unschedule(jobid) INTO result;

  IF result = true THEN
    DELETE FROM cron.job_name WHERE jobid = jobid;
  END IF;

  RETURN result;
END;
$$ LANGUAGE plpgsql STRICT SECURITY DEFINER;

CREATE OR REPLACE FUNCTION cron.update_unnamed(
  name text
, jobid bigint
) RETURNS boolean AS $$
#variable_conflict use_variable
BEGIN
  INSERT INTO cron.job_name (jobid, name) VALUES (jobid, name);
  RETURN true;
END;
$$ LANGUAGE plpgsql STRICT SECURITY DEFINER;

COMMIT;
